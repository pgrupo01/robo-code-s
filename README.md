# Talent Sprint - Solutis
## Robot Arena 
### MrRobot*
Esse projeto demonstra a criação de um robô utilizando o Robocode e desenvolvido na linguagem de programação JAVA. 

### Pré- Requisitos
Para execução do projeto foi necessário instalar os seguintes programas: 
[JAVA 8: Necessário para executar o Robocode.](https://java.com/en/download/)
[Robocode: Para desenvolver e testar o robô.](https://sourceforge.net/projects/robocode/files/)
[Git: Para salvar o repositorio e as atualizações feitas no projeto.](https://git-scm.com/) 

### Desenvolvimento 
Seguindo as instruções de criação [meu primeiro robô](https://robowiki.net/wiki/Robocode/My_First_Robot) do RoboWiki um novo robô pode ser criado. Após ser gerado o código padrão, como comentário, temos a [API](https://robocode.sourceforge.io/docs/robocode/robocode/Robot.html) que incluí todos os métodos e funções disponíveis para as modificações possíveis para o robô. Para o meu robô, utilizei o extends *Robot* e os seguintes métodos e funções:


**Métodos** | **Descrição**
-------- | --------- 
 run | Método principal do robô, pode incluir a movimentação, giro do radar e armas
 onScannedRobot | Método chamado quando a varredura do radar encontra um robô
 onHitByBullet | Método chamado quando o robô é atingido por uma bala
 onHitWall | Método chamado quando o robô colide com uma parede
 onHitRobot | Método chamado quando seu robô colide com outro robô
 onWin | Método chamado quando o robô ganha
 onDeath | Método chamado quando o robô é destruído
 
 
 **Funções** | **Descrição**
 ------- | --------- 
 setColors(Color.bodyColor,Color.gunColor,Color.radarColor) | Define a cor do robô, arma e radar
 ahead(double distancia) | Move o robô para frente
 back(double distancia) | Move o robô para trás
 fire() | Dispara uma bala, tem três potencias, 1, 2 e 3
 turnLeft(double graus) | Gira o robô para esquerda
 turnRight(double graus) | Gira o robô para direita 
 turnGunRight(double graus) | Vira a arma do robo para direita
 getDistance() | Retorna a distância do robo
 getBearing() | Retorna o ângulo do robô
 getEnergy() | Retorna a energia atual do robô
 
 ### Considerações
 O método run() tem sido o meu maior desafio, inicialmente testei o meu robô com o robô sample Interactive, parecia que a movimentação estava boa. Depois de finalizar todos os métodos, comecei a testar o meu robô com outros da classe sample como o Crazy, myFirstJuniorRobot e ele não sobrevivia aos rounds na arena, apesar de não ficar em último lugar no ranking. Testei algumas outras formas de movimentação, mas ainda assim não obtive sucesso. Vou continuar pesquisando e testando para ver onde consigo chegar.
 Em contrapartida, ter estudado o básico de Java na faculdade me auxiliou bastante para entender a construção do código padrão e como eu poderia fazer alterações e aplicar uma lógica diferente. Ainda aconteceu alguns erros, mas a documentação do Robocode e artigos me ajudaram a repará-los. 
 O método onHitRobot() foi o que eu achei bem interessante, é possível fazer várias estratégias diferentes e obter sucesso. 
 


