package Solutis_Robo;
import robocode.*;
import java.awt.Color;

// API help : https://robocode.sourceforge.io/docs/robocode/robocode/Robot.html

public class MrRobot extends Robot 
{
	//comportamentos
	public void run() {	//movimentação
		setColors(Color.black,Color.black,Color.white); // cores do tanque, arma e radar
		setBulletColor(Color green) //cor das balas do robô.
		while(true) {
			turnGunRight(360); //gira a arma 
			ahead(100); // anda 100px para frente 
			turnRight(90); //vira para direita em 90°
			turnGunRight(360); //gira a arma 
			ahead(100); //anda para frente 100px
			turnLeft(90); //vira para esquerda em 90° 
		}
	}

	public void onScannedRobot(ScannedRobotEvent e) { //quando ele scaneia outro robo
		double distancia = e.getDistance();
		//se o robo estiver com uma distancia de até 150px dá um tiro potencia 2 
		if (distancia < 150){
			fire(2); 
		}
		else { //caso contrário, um tiro só
			fire(1);
		}
	}

	public void onHitByBullet(HitByBulletEvent e) { //quando for atingido por uma bala 
		turnRight(90); //vira pra direita em 90°
		ahead(100); //anda para frente 100px
	}
	
	public void onHitWall(HitWallEvent e) { //quando colide com uma parede 
		back (60); //volta 60px
		turnRight(90); //gira para direita
		ahead(130); //anda pra frente 130px
	}
	
	public void onHitRobot(HitRobotEvent e){ //quando o robô colide com outro robô
		double angulo = e.getBearing(); //variavel que retorna o ang. do robô inimigo		
		//vai girar a arma para o angulo do robô e dar um tiro	
		turnRight(angulo);
		//se a energia do robô estiver maior que 70, potencia 3
		if (getEnergy() > 70){
			fire(3);
			back(100);
		}
		else { //caso contrário, potencia 1
			fire(1);
			back(100);
		}	
	}
	
	public void onWin(WinEvent e) { //quando o robô ganha
		turnRight(360000); //giradinha
	} 
	
	public void onDeath(DeathEvent e){ //quando o robô perde
		System.out.println(getName()+" foi eliminado!"); //mostra uma mensagem no console
	}
}